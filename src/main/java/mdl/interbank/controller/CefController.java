package mdl.interbank.controller;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import feign.FeignException;
import mdl.interbank.entity.ClienteRequest;
import mdl.interbank.entity.EstadoFinanciero;
import mdl.interbank.entity.LogMDL;
import mdl.interbank.entity.ResponseCefCabecera;
import mdl.interbank.service.EstadoFInancieroService;
import mdl.interbank.service.LogMDLService;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("api/cef")
public class CefController {

	@Autowired
	private EstadoFInancieroService serviceCEF;

	@Autowired
	private LogMDLService logservice;

	@GetMapping
	public ResponseEntity<?> prueba() {
		return ResponseEntity.ok("ok");
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarCEF(@Valid @RequestBody ClienteRequest clientrequest, BindingResult result) {

		if (result.hasErrors()) {
			return this.validar(result);
		}

		List<EstadoFinanciero> estadosfinancieros = serviceCEF.findByCodigounico(clientrequest.getCodeId());

		if (clientrequest.getNumsolicitud().equals("dummy")) {

			if (estadosfinancieros.isEmpty())
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body("No se encontraron registros con los valores solicitados");

			return ResponseEntity.ok(estadosfinancieros);
		}

		Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Format formatterMessage = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBasicAuth("uBseGenSit", "Ibkubssit16+");
		headers.add("X-INT-Device-Id", "0");
		headers.add("APIm-Debug", "true");
		headers.add("X-INT-Timestamp", formatter.format(new Date()));
		headers.add("X-INT-Service-Id", "ATM");
		headers.add("X-INT-Net-Id", "IS");
		headers.add("X-IBM-Client-Id", "489f5b95-2507-48e8-839a-b4424ef7e447");
		headers.add("X-INT-Supervisor-Id", "SXFL0000");
		headers.add("X-INT-Consumer-Id", "ATM");
		headers.add("X-INT-Branch-Id", "100");
		headers.add("X-INT-Message-Id", formatterMessage.format(new Date()));
		headers.add("X-INT-User-Id", "BSE0000");

		ResponseCefCabecera lstCEF = new ResponseCefCabecera();

		try {
			lstCEF = serviceCEF.listarCEF(headers, clientrequest);
		} catch (FeignException feigne) {
			String errFeign = (feigne.contentUTF8().length() > 0 ? feigne.contentUTF8() : feigne.getMessage());

			logservice.save(
					new LogMDL("num solicitud: " + clientrequest.getNumsolicitud().toString() + " -- " + errFeign));
			return new ResponseEntity<>(feigne.contentUTF8(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(estadosfinancieros.isEmpty()) {
			serviceCEF.saveAll(lstCEF.getTable());
		}

		return ResponseEntity.ok(lstCEF.getTable());

	}

	protected ResponseEntity<?> validar(BindingResult result) {
		Map<String, Object> errores = new HashMap<>();
		result.getFieldErrors().forEach(err -> {
			errores.put(err.getField(), "El campo " + err.getField() + " " + err.getDefaultMessage());
		});
		return ResponseEntity.badRequest().body(errores);
	}

	protected ResponseEntity<?> returnDummy() {

		String json = "{\"Table\": [\r\n" + "      {\r\n" + "      \"codeID\": \"0050002375\",\r\n"
				+ "      \"codeTypeEEFF\": \"1\",\r\n" + "      \"typeEEFF\": \"AUDITADO\",\r\n"
				+ "      \"dateEEFF\": \"31/12/2020\",\r\n" + "      \"currencyCode\": \"1\",\r\n"
				+ "      \"currency\": \"SOLES\",\r\n" + "      \"netSales\": \"5000.000000\",\r\n"
				+ "      \"EBITDA\": \"4900.000000\",\r\n" + "      \"heritage\": \"0.000000\",\r\n"
				+ "      \"currentAssets\": \"4000.000000\",\r\n" + "      \"currentPasives\": \"1000.000000\",\r\n"
				+ "      \"salesExpenses\": \"50.000000\",\r\n" + "      \"admExpenses\": \"0.000000\",\r\n"
				+ "      \"financialExpenses\": \"0.000000\",\r\n" + "      \"depreciation\": \"100.000000\",\r\n"
				+ "      \"amortization\": \"0.000000\",\r\n" + "      \"banksCashier\": \"1000.000000\",\r\n"
				+ "      \"bankDebtOverdraft\": \"1000.000000\",\r\n"
				+ "      \"partCteLongTermDebt\": \"0.000000\",\r\n" + "      \"intangibleAsset\": \"0.000000\",\r\n"
				+ "      \"netProfit\": \"4800.000000\",\r\n" + "      \"totalAssets\": \"5000.000000\",\r\n"
				+ "      \"totalCurrentLiabilities\": \"1000.000000\",\r\n"
				+ "      \"workingCapital\": \"0.000000\",\r\n" + "      \"totalActive\": \"5000.000000\",\r\n"
				+ "      \"totalLiabilities\": \"1000.000000\",\r\n"
				+ "      \"otherOperatingIncome\": \"0.000000\",\r\n" + "      \"costOfSales\": \"50.000000\",\r\n"
				+ "      \"totalNonCurrentLiabilities\": \"0.000000\",\r\n"
				+ "      \"longTermBankDebt\": \"0.000000\",\r\n" + "      \"longTermFinancialDebt\": \"0.000000\"\r\n"
				+ "   },\r\n" + "      {\r\n" + "      \"codeID\": \"0050002375\",\r\n"
				+ "      \"codeTypeEEFF\": \"3\",\r\n" + "      \"typeEEFF\": \"SITUACION\",\r\n"
				+ "      \"dateEEFF\": \"30/06/2018\",\r\n" + "      \"currencyCode\": \"1\",\r\n"
				+ "      \"currency\": \"SOLES\",\r\n" + "      \"netSales\": \"1900990.000000\",\r\n"
				+ "      \"EBITDA\": \"1900990.000000\",\r\n" + "      \"heritage\": \"0.000000\",\r\n"
				+ "      \"currentAssets\": \"1420.000000\",\r\n" + "      \"currentPasives\": \"1136.000000\",\r\n"
				+ "      \"salesExpenses\": \"0.000000\",\r\n" + "      \"admExpenses\": \"0.000000\",\r\n"
				+ "      \"financialExpenses\": \"70.000000\",\r\n" + "      \"depreciation\": \"453.000000\",\r\n"
				+ "      \"amortization\": \"0.000000\",\r\n" + "      \"banksCashier\": \"217.000000\",\r\n"
				+ "      \"bankDebtOverdraft\": \"182.000000\",\r\n"
				+ "      \"partCteLongTermDebt\": \"497.000000\",\r\n" + "      \"intangibleAsset\": \"0.000000\",\r\n"
				+ "      \"netProfit\": \"1900293.000000\",\r\n" + "      \"totalAssets\": \"4824.000000\",\r\n"
				+ "      \"totalCurrentLiabilities\": \"1136.000000\",\r\n"
				+ "      \"workingCapital\": \"1400.000000\",\r\n" + "      \"totalActive\": \"4824.000000\",\r\n"
				+ "      \"totalLiabilities\": \"496534.000000\",\r\n"
				+ "      \"otherOperatingIncome\": \"0.000000\",\r\n" + "      \"costOfSales\": \"0.000000\",\r\n"
				+ "      \"totalNonCurrentLiabilities\": \"495398.000000\",\r\n"
				+ "      \"longTermBankDebt\": \"495398.000000\",\r\n"
				+ "      \"longTermFinancialDebt\": \"0.000000\"\r\n" + "   }\r\n" + "]}";

		return ResponseEntity.ok(json);
	}

}
