package mdl.interbank.controller;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import feign.FeignException;
import mdl.interbank.entity.ClienteRequest;
import mdl.interbank.entity.Garantia;
import mdl.interbank.entity.LogMDL;
import mdl.interbank.entity.ResponseGacCabecera;
import mdl.interbank.service.GarantiaService;
import mdl.interbank.service.LogMDLService;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@RequestMapping("api/gac")
public class GacController {

	@Autowired
	private GarantiaService garantiaservice;

	@Autowired
	private LogMDLService logservice;

	@GetMapping
	public ResponseEntity<?> prueba() {
		return ResponseEntity.ok("ok");
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ObtenerGAC(@Valid @RequestBody ClienteRequest clientrequest, BindingResult result) {

		if (result.hasErrors()) {
			return this.validar(result);
		}

		List<Garantia> garantias = garantiaservice.findByCodeid(clientrequest.getCodeId());

		if (clientrequest.getNumsolicitud().equals("dummy")) {
			if (garantias.isEmpty())
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body("No se encontraron registros con los valores solicitados");
			return ResponseEntity.ok(garantias);
		}

		Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Format formatterMessage = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBasicAuth("uBseGenSit", "Ibkubssit16+");
		headers.add("X-INT-Device-Id", "0");
		headers.add("APIm-Debug", "true");
		headers.add("X-INT-Timestamp", formatter.format(new Date()));
		headers.add("X-INT-Service-Id", "ATM");
		headers.add("X-INT-Net-Id", "IS");
		headers.add("X-IBM-Client-Id", "489f5b95-2507-48e8-839a-b4424ef7e447");
		headers.add("X-INT-Supervisor-Id", "SXFL0000");
		headers.add("X-INT-Consumer-Id", "ATM");
		headers.add("X-INT-Branch-Id", "100");
		headers.add("X-INT-Message-Id", formatterMessage.format(new Date()));
		headers.add("X-INT-User-Id", "BSE0000");

		ResponseGacCabecera objGac = new ResponseGacCabecera();

		try {
			objGac = garantiaservice.listarGAC(headers, clientrequest);

			if (garantias.isEmpty()) {
				garantiaservice.saveAll(objGac.getDatos());
			}

		} catch (FeignException feigne) {
			String errFeign = (feigne.contentUTF8().length() > 0 ? feigne.contentUTF8() : feigne.getMessage());
			logservice.save(
					new LogMDL("num solicitud: " + clientrequest.getNumsolicitud().toString() + " -- " + errFeign));
			return new ResponseEntity<>(feigne.contentUTF8(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return ResponseEntity.ok(objGac.getDatos());

	}

	protected ResponseEntity<?> validar(BindingResult result) {
		Map<String, Object> errores = new HashMap<>();
		result.getFieldErrors().forEach(err -> {
			errores.put(err.getField(), "El campo " + err.getField() + " " + err.getDefaultMessage());
		});
		return ResponseEntity.badRequest().body(errores);
	}

	protected ResponseEntity<?> returnDummy() {

		String json = "{\"Datos\": [\r\n" + "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n"
				+ "      \"documentIdType\": \"2\",\r\n" + "      \"documentIdNumber\": \"20126709821\",\r\n"
				+ "      \"warrantyNumber\": \"0500076693\",\r\n" + "      \"documentCode\": \"19\",\r\n"
				+ "      \"documentName\": \"BIENES RAICES\",\r\n" + "      \"coverageTypeCode\": \"E\",\r\n"
				+ "      \"typeCoverage\": \"Especifica\",\r\n" + "      \"currencyCode\": \"1\",\r\n"
				+ "      \"currency\": \"DÃ³lar\",\r\n" + "      \"status\": \"VIGENTE\",\r\n"
				+ "      \"realizationValue\": \"15000\",\r\n" + "      \"commercialValue\": \"16000\",\r\n"
				+ "      \"taxValue\": \"17000\"\r\n" + "   },\r\n" + "      {\r\n"
				+ "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076696\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"BIENES RAICES\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"19000\",\r\n"
				+ "      \"commercialValue\": \"19000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076681\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"BIENES RAICES\",\r\n"
				+ "      \"coverageTypeCode\": \"G\",\r\n" + "      \"typeCoverage\": \"Sabana o Generica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"50000\",\r\n"
				+ "      \"commercialValue\": \"50000\",\r\n" + "      \"taxValue\": \"50000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076725\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"LOCAL INDUSTRIAL\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"15000\",\r\n"
				+ "      \"commercialValue\": \"16000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076786\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"TERRENO\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"78433\",\r\n"
				+ "      \"commercialValue\": \"79000\",\r\n" + "      \"taxValue\": \"83000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076580\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"CONCESION ELECTRICA\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"1000\",\r\n"
				+ "      \"commercialValue\": \"1000\",\r\n" + "      \"taxValue\": \"100\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076800\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"VIVIENDA URBANA\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"4000\",\r\n"
				+ "      \"commercialValue\": \"4000\",\r\n" + "      \"taxValue\": \"4000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076715\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"BIENES RAICES\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"15000\",\r\n"
				+ "      \"commercialValue\": \"16000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076792\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"VIVIENDA URBANA\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"3900\",\r\n"
				+ "      \"commercialValue\": \"3920\",\r\n" + "      \"taxValue\": \"3955\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076572\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"INMUEBLES\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"25000\",\r\n"
				+ "      \"commercialValue\": \"25000\",\r\n" + "      \"taxValue\": \"25000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076629\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"BIENES RAICES\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"15000\",\r\n"
				+ "      \"commercialValue\": \"16000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076698\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"BIENES RAICES\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"86000\",\r\n"
				+ "      \"commercialValue\": \"86000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076720\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"LOCAL INDUSTRIAL\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"15000\",\r\n"
				+ "      \"commercialValue\": \"16000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076724\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"LOCAL INDUSTRIAL\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"15000\",\r\n"
				+ "      \"commercialValue\": \"16000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076774\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"TERRENO\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"2\",\r\n" + "      \"currency\": \"Soles\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"150000\",\r\n"
				+ "      \"commercialValue\": \"151000\",\r\n" + "      \"taxValue\": \"152000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076605\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"BIENES RAICES\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"15000\",\r\n"
				+ "      \"commercialValue\": \"16000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076695\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"BIENES RAICES\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"15000\",\r\n"
				+ "      \"commercialValue\": \"16000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076587\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"VIVIENDA URBANA\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"50000\",\r\n"
				+ "      \"commercialValue\": \"50000\",\r\n" + "      \"taxValue\": \"50000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076539\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"VIVIENDA URBANA\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"20000\",\r\n"
				+ "      \"commercialValue\": \"20000\",\r\n" + "      \"taxValue\": \"20000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076582\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"CONCESION ELECTRICA\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"1000\",\r\n"
				+ "      \"commercialValue\": \"1000\",\r\n" + "      \"taxValue\": \"100\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076603\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"BIENES RAICES\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"15000\",\r\n"
				+ "      \"commercialValue\": \"16000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076727\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"BIENES RAICES\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"15000\",\r\n"
				+ "      \"commercialValue\": \"16000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076798\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"PREDIO AGRICOLA\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"3200\",\r\n"
				+ "      \"commercialValue\": \"3300\",\r\n" + "      \"taxValue\": \"3400\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076818\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"BIENES RAICES\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"2\",\r\n" + "      \"currency\": \"Soles\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"2100390.51\",\r\n"
				+ "      \"commercialValue\": \"2100390.51\",\r\n" + "      \"taxValue\": \"2100390.51\"\r\n"
				+ "   },\r\n" + "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n"
				+ "      \"documentIdType\": \"2\",\r\n" + "      \"documentIdNumber\": \"20126709821\",\r\n"
				+ "      \"warrantyNumber\": \"0500076611\",\r\n" + "      \"documentCode\": \"19\",\r\n"
				+ "      \"documentName\": \"LOCAL COMERCIAL Y OFICINAS\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"2\",\r\n" + "      \"currency\": \"Soles\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"135000\",\r\n"
				+ "      \"commercialValue\": \"135000\",\r\n" + "      \"taxValue\": \"135000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076606\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"LOCAL INDUSTRIAL\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"15000\",\r\n"
				+ "      \"commercialValue\": \"16000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076694\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"BIENES RAICES\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"15000\",\r\n"
				+ "      \"commercialValue\": \"16000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076697\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"BIENES RAICES\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"1\",\r\n" + "      \"currency\": \"DÃ³lar\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"15000\",\r\n"
				+ "      \"commercialValue\": \"16000\",\r\n" + "      \"taxValue\": \"17000\"\r\n" + "   },\r\n"
				+ "      {\r\n" + "      \"codeID\": \"0050000029\",\r\n" + "      \"documentIdType\": \"2\",\r\n"
				+ "      \"documentIdNumber\": \"20126709821\",\r\n" + "      \"warrantyNumber\": \"0500076810\",\r\n"
				+ "      \"documentCode\": \"19\",\r\n" + "      \"documentName\": \"VIVIENDA URBANA\",\r\n"
				+ "      \"coverageTypeCode\": \"E\",\r\n" + "      \"typeCoverage\": \"Especifica\",\r\n"
				+ "      \"currencyCode\": \"2\",\r\n" + "      \"currency\": \"Soles\",\r\n"
				+ "      \"status\": \"VIGENTE\",\r\n" + "      \"realizationValue\": \"1392590.1099999999\",\r\n"
				+ "      \"commercialValue\": \"1392590.1099999999\",\r\n"
				+ "      \"taxValue\": \"1392590.1099999999\"\r\n" + "   }\r\n" + "]}";

		return ResponseEntity.ok(json);
	}

}
